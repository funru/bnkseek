package com.test.bnkseek;

import com.test.bnkseek.CustomTableCell.DateTableCell;
import com.test.bnkseek.init.InitBnkseek;
import com.test.bnkseek.model.BNKSEEK;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.text.SimpleDateFormat;
import java.util.*;

public class Main  extends Application {

    private static FilteredList<BNKSEEK> filteredBNKSEEK;

    private static ObservableList<BNKSEEK> bnkseeks;

    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd MM yyyy");

    private TableView < BNKSEEK > table;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() throws Exception {
        super.init();
        bnkseeks = InitBnkseek.init();
        filteredBNKSEEK = new FilteredList<>(bnkseeks, p -> true);
    }
    @Override
    public void start(Stage primaryStage){
        table = new TableView<>();
        table.setEditable(true);
        TableColumn<BNKSEEK, String> pznCol//
                = new TableColumn<>("PZN");
        pznCol.setCellFactory(ComboBoxTableCell.forTableColumn(InitBnkseek.pznMap.values().toArray(new String[InitBnkseek.pznMap.values().size()])));
        pznCol.setOnEditCommit(
                t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setPzn(t.getNewValue())
        );
        TableColumn<BNKSEEK, String> uerCol//
                = new TableColumn<>("UER");
        uerCol.setCellFactory(ComboBoxTableCell.forTableColumn(InitBnkseek.uerMap.values().toArray(new String[InitBnkseek.uerMap.values().size()])));
        uerCol.setOnEditCommit(
                t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setUer(t.getNewValue())
        );
        TableColumn<BNKSEEK, String> rgnCol//
                = new TableColumn<>("RGN");
        rgnCol.setCellFactory(ComboBoxTableCell.forTableColumn(InitBnkseek.regMap.values().toArray(new String[InitBnkseek.regMap.values().size()])));
        rgnCol.setOnEditCommit(
                t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setRgn(t.getNewValue())
        );
        TableColumn<BNKSEEK, String> tnpCol//
                = new TableColumn<>("TNP");
        tnpCol.setCellFactory(ComboBoxTableCell.forTableColumn(InitBnkseek.tnpMap.values().toArray(new String[InitBnkseek.tnpMap.values().size()])));
        tnpCol.setOnEditCommit(
                t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setTnp(t.getNewValue())
        );
        TableColumn<BNKSEEK, String> newnumCol//
                = new TableColumn<>("NEWNUM");
        newnumCol.setCellFactory(TextFieldTableCell.forTableColumn());
        newnumCol.setOnEditCommit(event -> {
            String oldValue = event.getOldValue();
            String newValue = event.getNewValue();
            if(checkUniqueness(newValue)){
                event.getRowValue().setNewnum(newValue);
            }else {
                showAlertWithDefaultHeaderText();
                event.getRowValue().setNewnum(oldValue);
            }
            newnumCol.setVisible(false);
            newnumCol.setVisible(true);
        });
        TableColumn<BNKSEEK, Date> dtIzmCol//
                = new TableColumn<>("DT_IZM");
        dtIzmCol.setCellFactory(param -> new DateTableCell(filteredBNKSEEK));
        dtIzmCol.setOnEditCommit(
                t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setDtIzm(t.getNewValue())
        );
        TableColumn<BNKSEEK, Date> dtInCol//
                = new TableColumn<>("DT_IN");
        dtInCol.setCellFactory(param -> new DateTableCell(filteredBNKSEEK));
        dtInCol.setOnEditCommit(
                t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setDateIn(t.getNewValue())
        );
        TableColumn<BNKSEEK, Date> dtChCol//
                = new TableColumn<>("DT_CH");
        dtChCol.setCellFactory(param -> new DateTableCell(filteredBNKSEEK));
        dtChCol.setOnEditCommit(
                t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setDateCh(t.getNewValue())
        );
        newnumCol.setCellValueFactory(new PropertyValueFactory<>("newnum"));
        pznCol.setCellValueFactory(new PropertyValueFactory<>("pzn"));
        uerCol.setCellValueFactory(new PropertyValueFactory<>("uer"));
        rgnCol.setCellValueFactory(new PropertyValueFactory<>("rgn"));
        tnpCol.setCellValueFactory(new PropertyValueFactory<>("tnp"));
        dtIzmCol.setCellValueFactory(new PropertyValueFactory<>("dtIzm"));
        dtInCol.setCellValueFactory(new PropertyValueFactory<>("dateIn"));
        dtChCol.setCellValueFactory(new PropertyValueFactory<>("dateCh"));
        FilteredList<BNKSEEK> list = filteredBNKSEEK;
        Label filterLabelNewNum = new Label();
        filterLabelNewNum.setText("----Filter by BIK----");
        ObservableList<String> options = FXCollections.observableArrayList("");
        options.addAll(FXCollections.observableArrayList(InitBnkseek.pznMap.values()));
        final ComboBox filterFieldPzn = new ComboBox(options);
        TextField filterFieldNewNum = new TextField();
        TextField filterFieldRgn = new TextField();
        filterFieldNewNum.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredBNKSEEK.setPredicate(bnkseek -> {
                String rgnFilter = filterFieldRgn.getText();
                if (!rgnFilter.isEmpty() && !bnkseek.getRgn().toLowerCase().contains(rgnFilter.toLowerCase())) {
                    return false;
                }
                Object pznFilter = filterFieldPzn.getValue();
                if (pznFilter!=null && !pznFilter.toString().isEmpty() && !bnkseek.getPzn().toLowerCase().contains(pznFilter.toString().toLowerCase())) {
                    return false;
                }

                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();

                if (bnkseek.getNewnum().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });
        Label filterLabelRegion = new Label();
        filterLabelRegion.setText("---Filter by Region--");
        filterFieldRgn.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredBNKSEEK.setPredicate(bnkseek -> {
                String newnumFilter = filterFieldNewNum.getText();
                if (!newnumFilter.isEmpty() && !bnkseek.getNewnum().toLowerCase().contains(newnumFilter.toLowerCase())) {
                    return false;
                }
                Object pznFilter = filterFieldPzn.getValue();
                if (pznFilter!=null && !pznFilter.toString().isEmpty() &&  !bnkseek.getPzn().toLowerCase().contains(pznFilter.toString().toLowerCase())) {
                    return false;
                }

                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();

                if (bnkseek.getRgn().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });
        Label filterLabelPZN = new Label();
        filterLabelPZN.setText("---Filter by PZN---");
        filterFieldPzn.valueProperty().addListener((observable, oldValue, newValue) -> {
            filteredBNKSEEK.setPredicate(bnkseek -> {
                String newnumFilter = filterFieldNewNum.getText();
                if (!newnumFilter.isEmpty() && !bnkseek.getNewnum().toLowerCase().contains(newnumFilter.toLowerCase())) {
                    return false;
                }
                String rgnFilter = filterFieldRgn.getText();
                if (!rgnFilter.isEmpty() && !bnkseek.getRgn().toLowerCase().contains(rgnFilter.toLowerCase())) {
                    return false;
                }
                if (newValue == null || newValue.toString().isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toString().toLowerCase();

                if (bnkseek.getPzn().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });
        table.setItems(list);
        table.getColumns().addAll(newnumCol);
        table.getColumns().addAll(createStringColumn("real","ind","tnp","nnp","adr","rkc","namep","telef","regn","okpo","ksnp"));
        table.getColumns().addAll(pznCol, uerCol, rgnCol,tnpCol,dtIzmCol,dtInCol,dtChCol);
        final Button deleteButton = new Button("Delete row");
        deleteButton.setOnAction(new DeleteButtonListener());
        final Button addButton = new Button("Add row");
        addButton.setOnAction(new AddButtonListener());
        final FlowPane controls = new FlowPane();
        controls.setAlignment(Pos.CENTER);
        controls.getChildren().addAll(deleteButton,addButton);
        final FlowPane bikFilter = new FlowPane();
        bikFilter.setAlignment(Pos.CENTER);
        bikFilter.getChildren().addAll(filterLabelNewNum,filterFieldNewNum);
        final FlowPane rgnFilter = new FlowPane();
        rgnFilter.setAlignment(Pos.CENTER);
        rgnFilter.getChildren().addAll(filterLabelRegion,filterFieldRgn);
        final FlowPane pznFilter = new FlowPane();
        pznFilter.setAlignment(Pos.CENTER);
        pznFilter.getChildren().addAll(filterLabelPZN,filterFieldPzn);
        VBox vbox = new VBox(20);
        vbox.setPadding(new Insets(25, 25, 25, 25));;
        vbox.getChildren().addAll(controls,bikFilter,rgnFilter,pznFilter,table);
        primaryStage.setTitle("TableView BNKSEEK");
        Scene scene = new Scene(vbox, 1450, 1300);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void showAlertWithDefaultHeaderText() {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("newnum should be unique");
        alert.setContentText("This newnum is exist");
        alert.showAndWait();
    }

    public boolean checkUniqueness(String value) {
        return filteredBNKSEEK
                .stream()
                .noneMatch(item -> item.getNewnum().equals(value));
    }

    private class DeleteButtonListener implements EventHandler {

        @Override
        public void handle(Event event) {
            BNKSEEK selectedItem = table.getSelectionModel().getSelectedItem();
            bnkseeks.remove(selectedItem);
        }
    }

    private class AddButtonListener implements EventHandler {

        @Override
        public void handle(Event event) {
            BNKSEEK bnkseek = new BNKSEEK();
            bnkseeks.add(0,bnkseek);
            int row = 0;
            table.requestFocus();
            table.getSelectionModel().select(row);
            table.getFocusModel().focus(row);
        }
    }

    private TableColumn<BNKSEEK, String>[] createStringColumn(String... nameColumns){
        TableColumn<BNKSEEK,String>[] columns = new TableColumn[nameColumns.length];
        for (int i = 0; i < nameColumns.length; i++) {
            TableColumn<BNKSEEK, String> column //
                    = new TableColumn<>(nameColumns[i]);
            column.setCellFactory(TextFieldTableCell.forTableColumn());
            column.setOnEditCommit(
                    t -> t.getTableView().getItems().get(
                            t.getTablePosition().getRow()).setReal(t.getNewValue())
            );
            column.setCellValueFactory(new PropertyValueFactory<>(nameColumns[i]));
            columns[i]=column;
        }
        return columns;
    }
}
