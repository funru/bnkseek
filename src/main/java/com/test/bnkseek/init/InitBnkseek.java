package com.test.bnkseek.init;

import com.linuxense.javadbf.DBFException;
import com.linuxense.javadbf.DBFField;
import com.linuxense.javadbf.DBFReader;
import com.test.bnkseek.model.BNKSEEK;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class InitBnkseek {
    public static Map<String,String> pznMap = getMapFromDBF("dbf/PZN.DBF",1,3);
    public static Map<String,String> tnpMap = getMapFromDBF("dbf/TNP.DBF",1,2);
    public static Map<String,String> uerMap = getMapFromDBF("dbf/UER.DBF",1,2);
    public static Map<String,String> regMap = getMapFromDBF("dbf/REG.DBF",1,2);

    public static ObservableList<BNKSEEK> init(){
        ObservableList<BNKSEEK> rows = FXCollections.observableArrayList();
        try {
            InputStream inputStream = new FileInputStream("dbf/BNKSEEK.DBF");
            DBFReader reader = new DBFReader(inputStream);
            reader.setCharactersetName("Cp866");
            int numberOfFields = reader.getFieldCount();
            for (int i = 0; i < numberOfFields; i++) {
                DBFField field = reader.getField(i);
                System.out.println(field.getName());
            }
            Object[] rowObjects;
            while ((rowObjects = reader.nextRecord()) != null) {
                    String real = rowObjects[1].toString();
                    String pzn = pznMap.get(rowObjects[2].toString());
                    String uer = uerMap.get(rowObjects[3].toString());
                    String rgn = regMap.get(rowObjects[4].toString());
                    String ind = rowObjects[5].toString();
                    String tnp = tnpMap.get(rowObjects[6].toString().trim());
                    String nnp = rowObjects[7].toString();
                    String adr = rowObjects[8].toString();
                    String rkc = rowObjects[9].toString();
                    String namep = rowObjects[10].toString();
                    String newnum = rowObjects[12].toString();
                    String telef = rowObjects[18].toString();
                    String regn = rowObjects[19].toString();
                    String okpo = rowObjects[20].toString();
                    Date dtIzm = (Date) rowObjects[21];
                    String ksnp = rowObjects[23].toString();
                    Date dateIn = (Date) rowObjects[24];
                    Date dateCh = (Date) rowObjects[25];
                    rows.add(new BNKSEEK(real, pzn, uer, rgn, ind, tnp, nnp, adr, rkc, namep, newnum, telef, regn, okpo, dtIzm, ksnp, dateIn, dateCh));
            }
            inputStream.close();
        } catch (DBFException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return rows;
    }

    private static Map<String,String> getMapFromDBF(String fileName, int rowKey, int rowValue) {
        Map<String,String> result = new HashMap<>();
        try {
            InputStream inputStream = new FileInputStream(fileName);
            DBFReader reader = new DBFReader(inputStream);
            reader.setCharactersetName("Cp866");
            int numberOfFields = reader.getFieldCount();
            for (int i = 0; i < numberOfFields; i++) {
                DBFField field = reader.getField(i);
                System.out.println(field.getName());
            }
            Object[] rowObjects;

            while ((rowObjects = reader.nextRecord()) != null) {
                result.put(rowObjects[rowKey].toString().trim(),rowObjects[rowValue].toString());
            }
            inputStream.close();
        } catch (DBFException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return  result;
    }
}
