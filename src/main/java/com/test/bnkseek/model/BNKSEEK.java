package com.test.bnkseek.model;

import java.util.Date;

public class BNKSEEK {

    private String real;
    private String pzn;
    private String uer;
    private String rgn;
    private String ind;
    private String tnp;
    private String nnp;
    private String adr;
    private String rkc;
    private String namep;
    private String newnum;
    private String telef;
    private String regn;
    private String okpo;
    private Date dtIzm;
    private String ksnp;
    private Date dateIn;
    private Date dateCh;

    public BNKSEEK(){
        this.real = "";
        this.pzn = "";
        this.uer = "";
        this.rgn = "";
        this.ind = "";
        this.tnp = "";
        this.nnp = "";
        this.adr = "";
        this.rkc = "";
        this.namep = "";
        this.newnum = "";
        this.telef = "";
        this.regn = "";
        this.okpo = "";
        this.dtIzm = new Date();
        this.ksnp = "";
        this.dateIn = new Date();
        this.dateCh = new Date();
    }

    public BNKSEEK(String real, String pzn, String uer, String rgn, String ind, String tnp, String nnp, String adr, String rkc, String namep, String newnum, String telef, String regn, String okpo, Date dtIzm, String ksnp, Date dateIn, Date dateCh) {
        this.real = real;
        this.pzn = pzn;
        this.uer = uer;
        this.rgn = rgn;
        this.ind = ind;
        this.tnp = tnp;
        this.nnp = nnp;
        this.adr = adr;
        this.rkc = rkc;
        this.namep = namep;
        this.newnum = newnum;
        this.telef = telef;
        this.regn = regn;
        this.okpo = okpo;
        this.dtIzm = dtIzm;
        this.ksnp = ksnp;
        this.dateIn = dateIn;
        this.dateCh = dateCh;
    }

    public String getReal() {
        return real;
    }

    public void setReal(String real) {
        this.real = real;
    }

    public String getPzn() {
        return pzn;
    }

    public void setPzn(String pzn) {
        this.pzn = pzn;
    }

    public String getUer() {
        return uer;
    }

    public void setUer(String uer) {
        this.uer = uer;
    }

    public String getRgn() {
        return rgn;
    }

    public void setRgn(String rgn) {
        this.rgn = rgn;
    }

    public String getInd() {
        return ind;
    }

    public void setInd(String ind) {
        this.ind = ind;
    }

    public String getTnp() {
        return tnp;
    }

    public void setTnp(String tnp) {
        this.tnp = tnp;
    }

    public String getNnp() {
        return nnp;
    }

    public void setNnp(String nnp) {
        this.nnp = nnp;
    }

    public String getAdr() {
        return adr;
    }

    public void setAdr(String adr) {
        this.adr = adr;
    }

    public String getRkc() {
        return rkc;
    }

    public void setRkc(String rkc) {
        this.rkc = rkc;
    }

    public String getNamep() {
        return namep;
    }

    public void setNamep(String namep) {
        this.namep = namep;
    }

    public String getNewnum() {
        return newnum;
    }

    public void setNewnum(String newnum) {
        this.newnum = newnum;
    }

    public String getTelef() {
        return telef;
    }

    public void setTelef(String telef) {
        this.telef = telef;
    }

    public String getRegn() {
        return regn;
    }

    public void setRegn(String regn) {
        this.regn = regn;
    }

    public String getOkpo() {
        return okpo;
    }

    public void setOkpo(String okpo) {
        this.okpo = okpo;
    }

    public Date getDtIzm() {
        return dtIzm;
    }

    public void setDtIzm(Date dtIzm) {
        this.dtIzm = dtIzm;
    }

    public String getKsnp() {
        return ksnp;
    }

    public void setKsnp(String ksnp) {
        this.ksnp = ksnp;
    }

    public Date getDateIn() {
        return dateIn;
    }

    public void setDateIn(Date dateIn) {
        this.dateIn = dateIn;
    }

    public Date getDateCh() {
        return dateCh;
    }

    public void setDateCh(Date dateCh) {
        this.dateCh = dateCh;
    }

    @Override
    public String toString() {
        return "BNKSEEK{" +
                "real='" + real + '\'' +
                ", pzn='" + pzn + '\'' +
                ", uer='" + uer + '\'' +
                ", rgn='" + rgn + '\'' +
                ", ind='" + ind + '\'' +
                ", tnp='" + tnp + '\'' +
                ", nnp='" + nnp + '\'' +
                ", adr='" + adr + '\'' +
                ", rkc='" + rkc + '\'' +
                ", namep='" + namep + '\'' +
                ", newnum='" + newnum + '\'' +
                ", telef='" + telef + '\'' +
                ", regn='" + regn + '\'' +
                ", okpo='" + okpo + '\'' +
                ", dtIzm=" + dtIzm +
                ", ksnp='" + ksnp + '\'' +
                ", dateIn=" + dateIn +
                ", dateCh=" + dateCh +
                '}';
    }
}
